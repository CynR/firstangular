import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { ListQuestionComponent } from './list-question/list-question.component';
import {QuestionComponent} from './question/question.component';
import { PeopleComponent } from './people/people.component';
import { ListPeopleComponent } from './list-people/list-people.component';
import {RouterModule, Routes} from '@angular/router';
import {HttpClientModule} from '@angular/common/http';
import {StarwarsService} from './service/starwars';
import { CreateQuestionComponent } from './create-question/create-question.component';
import {FormsModule} from '@angular/forms';

const appRoutes: Routes = [
  { path: 'listPeople', component: ListPeopleComponent},
  { path: 'questions', component: ListQuestionComponent}
  ];

@NgModule({
  declarations: [
    AppComponent,
    ListQuestionComponent,
    QuestionComponent,
    PeopleComponent,
    ListPeopleComponent,
    CreateQuestionComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    RouterModule.forRoot(
      appRoutes
      // <-- debugging purposes only
    ),
    FormsModule
  ],
  providers: [
    StarwarsService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
