import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {Question} from '../model/question';

function getMyRandom(max: number) {
  return Math.floor(Math.random() * Math.floor(max));
}

@Component({
  selector: 'app-create-question',
  templateUrl: './create-question.component.html',
  styleUrls: ['./create-question.component.css']
})
export class CreateQuestionComponent implements OnInit {

  questionCreated: Question;
  enonce = '';
  @Input() CorrectAnswer = '';
  @Input() WrongAnswer = '';
  showMsgQuestionInserted = false;
  imageQuestionInserted = 'Question inserted';
  showMsgErrorMissingInfo = false;
  imageErrorMissingInfo = 'PLEASE insert information in ALL the inputs';
  errorLength: string;
  showErrorLength = false;
  myRand: number;

  // emitter
  @Output() qCreated = new EventEmitter<Question>();
  @Output() myRandEmitted = new EventEmitter<any>();

  changeData() {
  //  console.log(this.enonce);
  }
  valider() {
    if (this.enonce.length === 0 || this.CorrectAnswer.length === 0 || this.WrongAnswer.length === 0) {
      console.log('error');
      this.showMsgErrorMissingInfo = true;
      this.showMsgQuestionInserted = false;
    } else {
      this.myRandEmitted.emit(this.myRand = getMyRandom(2));
      this.showMsgQuestionInserted = true;
      this.showMsgErrorMissingInfo = false;
      this.questionCreated = new Question(this.enonce, this.CorrectAnswer, this.WrongAnswer);
      this.qCreated.emit(this.questionCreated);
    }
  }

  disable() {
   /* if (this.enonce.length === 0 || this.reponse1.length === 0 || this.reponse2.length === 0) {
      this.showErrorMissingInfo = true;
      this.showMsgQuestionInserted = false;
    } */
  }

  constructor() { }

  ngOnInit() {
  }

}
