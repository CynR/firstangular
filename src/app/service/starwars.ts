import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {People} from '../model/people';
import {ListPeople} from '../model/list-people';
import {filter, map, switchMap} from 'rxjs/operators';

@Injectable()
export class StarwarsService {

  constructor(
    private http: HttpClient) {
  }

  getPeoples(): Observable<People> {
    return this.http.get<People>('https://swapi.co/api/people/1');
  }

 /* getListPeoples(): Observable<ListPeople> {
    return this.http.get<ListPeople>('https://swapi.co/api/people');
  } */

  getListPeoples(): Observable<People[]> {
    // pipe is just for observable
    return this.http.get<ListPeople>('https://swapi.co/api/people').pipe(
      map(listPeople => listPeople.results)
    );
  }

  getOne(): Observable<People[]> {
    // pipe is just for observable
    return this.http.get<ListPeople>('https://swapi.co/api/people').pipe(
      map(listPeople => listPeople.results)
      );
  }

}
