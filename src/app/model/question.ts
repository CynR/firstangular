export class Question {
  enonce: string;
  CorrectAnswer: string;
  WrongAnswer: string;

  constructor(enonce: string, CorrectAnswer: string, WrongAnswer: string) {
    this.enonce = enonce;
    this.CorrectAnswer = CorrectAnswer;
    this.WrongAnswer = WrongAnswer;
  }

}
