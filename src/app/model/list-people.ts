import {People} from './people';
import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';

@Injectable()
export class ListPeople {
  results: People[];

  constructor(
    private http: HttpClient) {
  }


}
