import { Component, OnInit } from '@angular/core';
import {People} from '../model/people';
import {StarwarsService} from '../service/starwars';

@Component({
  selector: 'app-list-people',
  templateUrl: './list-people.component.html',
  styleUrls: ['./list-people.component.css']
})
export class ListPeopleComponent implements OnInit {

  peopleList: People[];
  load = true;

  constructor(
    private swService: StarwarsService
  ) {
    this.swService.getListPeoples().subscribe(reponse => {
      this.peopleList = reponse;
      this.load = false;
  });
  }

  ngOnInit() {
  }
}
