import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {Question} from '../model/question';

@Component({
  selector: 'app-list-question',
  templateUrl: './list-question.component.html',
  styleUrls: ['./list-question.component.css']
})
export class ListQuestionComponent implements OnInit {

  questions: Question[] = [];
  myReceivedRandom: number;

  showQuestion(question: Question) {
    return true;
  }
  constructor() {
    this.questions.push(new Question('How are you ?', 'Here is the right answer', 'Here is the wrong answer'));
  }

  onQuestionCreated($event) {
    this.questions.push($event);
  }
  makingMyRandom($event) {
    this.myReceivedRandom = $event;
  }
  ngOnInit() {
  }
}


