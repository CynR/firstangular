import {Component, Input, OnInit} from '@angular/core';
import {People} from '../model/people';
import {StarwarsService} from '../service/starwars';
import {renderConstantPool} from '@angular/compiler-cli/ngcc/src/rendering/renderer';

@Component({
  selector: 'app-people',
  templateUrl: './people.component.html',
  styleUrls: ['./people.component.css']
})
export class PeopleComponent implements OnInit {

  @Input() people: People;

  constructor(
    private swService: StarwarsService
  ) {
    this.swService.getPeoples().subscribe(reponse => {
      console.log(reponse);
    });
  }

  ngOnInit() {
  }

}
