import {Component, Input, OnInit} from '@angular/core';
import {Question} from '../model/question';

function getMyRandom(max: number) {
  return Math.floor(Math.random() * Math.floor(max));
}

@Component({
  selector: 'app-question',
  templateUrl: './question.component.html',
  styleUrls: ['./question.component.css']
})
export class QuestionComponent implements OnInit {
  // Input allows to receive data
  @Input() enonce: string;
  @Input() CorrectAnswer: string;
  MsgCorrectAnswer = 'Correct Answer!';
  @Input() WrongAnswer: string;
  MsgWrongAnswer = 'Wrong Answer!';
  displayCorrectAnswer = false;
  displayWrongAnswer = false;
  // randomNumber made when the question is inserted (create-question.component)
  @Input() randomFinal;
  // used  to assign condition in question.html
  myRand = 0;

  // function that maybe can help change randomFinal in constant
  noChange(noChanging: number) {
    const noChangeRand = noChanging;
  }

  Validation(selectedAnswer: string) {
  if (selectedAnswer === this.CorrectAnswer) {
      this.displayCorrectAnswer = true;
      this.displayWrongAnswer = false;
  } else if (selectedAnswer === this.WrongAnswer) {
      this.displayWrongAnswer = true;
      this.displayCorrectAnswer = false;
    }
  }
  constructor() {
  }

  ngOnInit() {
  }

}
